package logutils

import (
	"github.com/logrusorgru/aurora"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

type decorator func(string) aurora.Value

// VulnerabilityFormatter is used to by the logrus package to provide a custom logutils
type VulnerabilityFormatter struct {
	Project string
}

// Format creates a custom log formatter so we can colorize and format the output of vulnerabilities
func (f *VulnerabilityFormatter) Format(entry *log.Entry) ([]byte, error) {
	severityLevel := entry.Data["SeverityLevel"]

	defaultColoredLogutilMsg, err := (&logutil.Formatter{Project: f.Project}).Format(entry)
	if err != nil {
		return nil, err
	}

	if severityLevel == nil {
		return defaultColoredLogutilMsg, nil
	}

	var logColor decorator
	switch severityLevel {
	case report.SeverityLevelUnknown:
		logColor = func(in string) aurora.Value { return aurora.BrightBlue(in) }
	case report.SeverityLevelInfo:
		logColor = func(in string) aurora.Value { return aurora.Green(in) }
	case report.SeverityLevelLow:
		logColor = func(in string) aurora.Value { return aurora.Yellow(in) }
	case report.SeverityLevelMedium:
		logColor = func(in string) aurora.Value { return aurora.Magenta(in) }
	case report.SeverityLevelHigh:
		logColor = func(in string) aurora.Value { return aurora.BrightRed(in) }
	case report.SeverityLevelCritical:
		logColor = func(in string) aurora.Value { return aurora.Red(in) }
	}

	// strip the ANSI color escape codes from the logutil formatted msg
	// if you don't want to output the `[INFO] [GitLab Depscan] [2020-07-07T22:46:27+10:00] ▶`
	// prefix here, then disregard the defaultLogutilMsg and use your own custom output format
	defaultLogutilMsg := string(defaultColoredLogutilMsg)[7:len(defaultColoredLogutilMsg)]
	coloredLogMsg := logColor(defaultLogutilMsg)

	return []byte(coloredLogMsg.String()), nil
}

// LogVulnerability logs a vulnerability - this function isn't necessary, LogVulnerability is only
// called in a single place, so we could just call `log.WithFields` there instead
func LogVulnerability(fmtstr string, level report.SeverityLevel, msg ...interface{}) {
	log.WithFields(logrus.Fields{
		"SeverityLevel": level,
	}).Infof(fmtstr, msg...)
}
