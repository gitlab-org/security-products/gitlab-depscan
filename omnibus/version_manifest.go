package omnibus

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// UsedDependency is a tupe that describes a dependency in terms of product name and version string
type UsedDependency struct {
	Name    string
	Vendor  string
	Version string
}

// VersionManifest is a version omnibus file from the GitLab Omnibus package
type VersionManifest struct {
	ManifestFormat int `json:"manifest_format"`
	Software       map[string]struct {
		Vendor         string `json:"vendor"`
		DisplayVersion string `json:"display_version"`
	} `json:"software"`
}

// ParseVersionManifest parsers a GitLab omnibus omnibus file
func ParseVersionManifest(path string) ([]UsedDependency, error) {
	manifestData, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return ProcessVersionManifestContent(string(manifestData))
}

// ProcessVersionManifestContent processes the content of a GitLab omnibus omnibus file
func ProcessVersionManifestContent(manifestString string) ([]UsedDependency, error) {
	var manifest VersionManifest
	err := json.Unmarshal([]byte(manifestString), &manifest)
	if err != nil {
		return nil, fmt.Errorf("processing version manifest content: %w", err)
	}

	usedDependencies := make([]UsedDependency, 0, len(manifest.Software))
	for depName, depMeta := range manifest.Software {
		usedDependencies = append(usedDependencies, UsedDependency{Name: depName, Vendor: depMeta.Vendor, Version: depMeta.DisplayVersion})
	}
	return usedDependencies, err
}
