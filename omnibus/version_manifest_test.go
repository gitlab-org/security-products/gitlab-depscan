package omnibus

import (
	"testing"
)

func TestExtractUsedVersionFromManifest(t *testing.T) {
	manifestString := `
{
		"manifest_format": 2,
		"software": {
			"nginx": {
				"locked_version": "1.2",
					"locked_source": {
					"md5": "8cb9c412e5f2d96bc6f459aa8c6282a1",
						"url": "http://ftp.gnu.org/gnu/ncurses/ncurses-5.9.tar.gz"
				},
				"display_version": "1.2",
				"source_type": "url",
					"described_version": "5.9",
					"license": "MIT"
			},
			"ncurses": {
				"locked_version": "5.9",
					"locked_source": {
					"md5": "8cb9c412e5f2d96bc6f459aa8c6282a1",
						"url": "http://ftp.gnu.org/gnu/ncurses/ncurses-5.9.tar.gz"
				},
				"display_version": "5.9",
				"source_type": "url",
					"described_version": "5.9",
					"license": "MIT"
			}
	},
	"build_version": "12.2.4+rnightly.125492.5032ed10",
	"build_git_revision": "5032ed10c547382d5ae19aea89d9f15dad202003",
	"license": "MIT"
}
`

	for _, tt := range []struct {
		in             string
		expectedResult []UsedDependency
	}{
		{
			manifestString,
			[]UsedDependency{
				{"nginx", "", "1.2"},
				{"ncurses", "", "5.9"},
			},
		},
	} {
		extractedVersions, err := ProcessVersionManifestContent(tt.in)
		if err != nil {
			t.Fatalf(err.Error())
		}

		res := true

		for i := range extractedVersions {
			innerres := false
			for k := range tt.expectedResult {
				if tt.expectedResult[k].Name == extractedVersions[i].Name &&
					tt.expectedResult[k].Version == extractedVersions[i].Version {
					innerres = true
					break
				}
			}
			res = res && innerres
		}
		if !res {
			t.Fatalf("got:%v, expected:%v", extractedVersions, tt.expectedResult)
		}
	}
}
