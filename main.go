package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"

	"gitlab-depscan/logutils"
	"gitlab-depscan/metadata"
	"gitlab-depscan/plugin"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "gitlab-depscan analyzer for GitLab Omnibus"
	app.Authors = []*cli.Author{{Name: metadata.AnalyzerVendor}}
	app.Version = metadata.AnalyzerVersion

	log.SetFormatter(&logutils.VulnerabilityFormatter{Project: metadata.AnalyzerName})

	app.Commands = command.NewCommands(command.Config{
		ArtifactName: command.ArtifactNameDependencyScanning,
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: []cli.Flag{
			&cli.StringFlag{
				Name:    metadata.DenyListParam,
				EnvVars: []string{metadata.DenyListEnv},
				Usage:   "Path to denylist file.",
			},
			&cli.StringFlag{
				Name:    metadata.ManifestParam,
				EnvVars: []string{metadata.ManifestEnv},
				Usage:   "filename of manifest file.",
			},
			&cli.StringFlag{
				Name:  metadata.NVDDirParam,
				Value: "/opt/nvd-mirror-json-data-v2",
				Usage: "path to NVD directory.",
			},
			&cli.BoolFlag{
				Name:    metadata.UseOmnibusPathsParam,
				EnvVars: []string{metadata.UseOmnibusPathsEnv},
				Usage:   "Set path to software definitions in report file.",
			},
		},
		Convert:  convert,
		Analyzer: metadata.AnalyzerDetails,
		Scanner:  metadata.ReportScanner,
		ScanType: report.CategoryDependencyScanning,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
