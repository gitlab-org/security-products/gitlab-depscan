package plugin

import (
	"os"
	"path/filepath"

	"gitlab-depscan/metadata"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"
)

// Match looks for files to trigger the gitlab-depscan analyzer
func Match(path string, info os.FileInfo) (bool, error) {
	filepath.Walk(path,
		func(path string, info os.FileInfo, err error) error {
			// automatically detect denylist and manifest file if possible
			switch info.Name() {
			case ".cveignore":
				os.Setenv(metadata.DenyListEnv, path)
			case "version-manifest.json":
				os.Setenv(metadata.ManifestEnv, path)
			}
			return nil
		})
	return true, nil
}

func init() {
	plugin.Register(metadata.AnalyzerName, Match)
}
