package metadata

import (
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

const (
	// UseOmnibusPathsEnv is an environment variable that can be used to enable/disable setting locations to software definitions
	UseOmnibusPathsEnv = "DEPSCAN_USE_OMNIBUS_PATHS"
	// UseOmnibusPathsParam is an command line parameter that can be used to enable/disable setting locations to software definitions
	UseOmnibusPathsParam = "use-omnibus-paths"
	// DenyListEnv is an environment variable that can used to set path to the denylist file
	DenyListEnv = "DENYLIST"
	// DenyListParam is a command line parameter that can used to set path to the denylist file
	DenyListParam = "denylist"
	// ManifestEnv is an environment variable that can used to set path to the manifest file
	ManifestEnv = "MANIFEST"
	// ManifestParam is a command line parameter that can used to set path to the manifest file
	ManifestParam = "manifest"
	// NVDDirParam is a command line parameter that can used to set path to the NVD directory
	NVDDirParam = "nvddir"
	// AnalyzerName is the name of gitlab-depscan as it appears in the output log
	AnalyzerName = "gitlab-depscan"
	// AnalyzerVendor is the vendor/maintainer of the analyzer
	AnalyzerVendor = "GitLab"
	// AnalyzerID identifies the analyzer
	AnalyzerID = AnalyzerName
	// scannerID identifies the scanner that generated the report
	scannerID = "gitlab-depscan"
	// scannerName identifies the scanner that generated the report
	scannerName = "GitLab Depscan"
	// AnalyzerURL identifier the URL of the analyser
	AnalyzerURL = "https://gitlab.com/gitlab-org/security-products/gitlab-depscan"
	// PackageManager is the name of the package manager
	PackageManager = "omnibus"

	scannerVendor = AnalyzerVendor
	scannerURL    = AnalyzerURL
)

var (
	// AnalyzerVersion identifies the version of this analyser
	AnalyzerVersion = "not-configured"
	// ScannerVersion identifies the version of this scanner
	ScannerVersion = &AnalyzerVersion
)

// AnalyzerDetails provides information about this scanner
var AnalyzerDetails = report.ScannerDetails{
	ID:   AnalyzerID,
	Name: AnalyzerName,
	URL:  AnalyzerURL,
	Vendor: report.Vendor{
		Name: AnalyzerVendor,
	},
	Version: AnalyzerVersion,
}

// IssueScanner describes the scanner used to find a vulnerability
var IssueScanner = report.Scanner{
	ID:   scannerID,
	Name: scannerName,
}

// ReportScanner returns identifying information about a security scanner
var ReportScanner = report.ScannerDetails{
	ID:      scannerID,
	Name:    scannerName,
	Version: *ScannerVersion,
	Vendor: report.Vendor{
		Name: scannerVendor,
	},
	URL: scannerURL,
}
