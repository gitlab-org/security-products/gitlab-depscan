# GitLab Depscan changelog

## v3.0.0
- Add native NVD 2.0 CVE matching (!31)

## v2.4.0
- Upgrade to Dependency Scanning report schema v15.0.4 (!29)
- Upgrade to Go 1.20 (!29)
- Upgrade nvd-mirror to the 1.3.1 tag(!29)

## v2.3.3
- Create `dependency_files` with Omnibus paths when the `DEPSCAN_USE_OMNIBUS_PATHS` env variable is `true`(!28)

## v2.3.2
-  No longer use exit code 1 when there are vulnerabilities (!27)

## v2.3.1
- Upgrade nvd-mirror to fix version parsing issue (!26)

## v2.3.0
- Upgrade to Dependency Scanning report schema v14.0.4 (!25)
- Upgrade to Go 1.17 (!25)

## v2.2.0
- Support for CVSS 3.1 (!23)
- Add proper file links to report (!24)

## v2.1.0
- Incorporate `vendor` field when searching for vulnerabilities in dependencies.

## v2.0.0
- Switch to `display_version` field in `version-manifest.json` to search for vulnerabilities.

## v1.1.0
- Added the `NVD_DB_UPDATE` environment variable to enable/disable automated nvd-mirror database updates (!16)

## v1.0.1
- Fix bug preventing execution of `nvd-mirror.sh` script by using absolute path (!15)

## v1.0.0
- Initial release
