package cvss

import (
	"strings"

	cvss3 "github.com/spiegel-im-spiegel/go-cvss/v3"
	cvss2 "github.com/umisama/go-cvss"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

// TODO: Use shared package for cvss parsing when https://gitlab.com/gitlab-org/gitlab/-/issues/227171
//       has been completed
// it would be great if we could isolate this as a go module at a certain point

// Severity parses the given cvss vectors and returns the issue.SeverityLevel value
func Severity(cvss string) report.SeverityLevel {
	if cvss == "" {
		return report.SeverityLevelUnknown
	}

	if strings.HasPrefix(cvss, "CVSS:3.0") || strings.HasPrefix(cvss, "CVSS:3.1") {
		m := cvss3.New()

		err := m.ImportBaseVector(cvss)
		if err == nil {
			return cvss3ScoreToSeverity(m.Base.Score())
		}
	}

	// fallback to cvss2
	v, err := cvss2.ParseVectors(parenthesizeCVSS2(cvss))
	if err != nil {
		return report.SeverityLevelUnknown
	}

	return cvss2ScoreToSeverity(v.BaseScore())
}

// cvss3ScoreToSeverity uses the CVSS v3.1 "Qualitative Severity Rating Scale" to convert the score to a
// severity level.
// See https://www.first.org/cvss/v3.1/specification-document#Qualitative-Severity-Rating-Scale for more details
func cvss3ScoreToSeverity(score float64) report.SeverityLevel {
	switch true {
	case score == 0:
		return report.SeverityLevelInfo
	case score > 0 && score < 4.0:
		return report.SeverityLevelLow
	case score >= 4.0 && score < 7.0:
		return report.SeverityLevelMedium
	case score >= 7.0 && score < 9.0:
		return report.SeverityLevelHigh
	case score >= 9.0:
		return report.SeverityLevelCritical
	default:
		return report.SeverityLevelUnknown
	}
}

// cvss2ScoreToSeverity uses the CVSS v2.0 Ratings scale to convert the score to a severity level.
// See https://nvd.nist.gov/vuln-metrics/cvss for more details
func cvss2ScoreToSeverity(score float64) report.SeverityLevel {
	switch true {
	case score >= 0 && score < 4.0:
		return report.SeverityLevelLow
	case score >= 4.0 && score < 7:
		return report.SeverityLevelMedium
	case score >= 7.0:
		return report.SeverityLevelHigh
	default:
		return report.SeverityLevelUnknown
	}
}

// CVSS2 vectors must be enclosed in parentheses
func parenthesizeCVSS2(inCVSS2 string) string {
	if inCVSS2 == "" {
		return ""
	}

	if inCVSS2[0:1] != "(" {
		inCVSS2 = "(" + inCVSS2
	}

	if inCVSS2[len(inCVSS2)-1:] != ")" {
		inCVSS2 = inCVSS2 + ")"
	}

	return inCVSS2
}
