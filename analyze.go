package main

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	config "gitlab-depscan/metadata"
	"gitlab-depscan/nvd"
	"gitlab-depscan/omnibus"
)

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// DENYLIST might be propagated here by the `match` function if it finds a matching denylist file,
	// so try that first
	denylist := os.Getenv(config.DenyListEnv)
	if f := c.String(config.DenyListParam); f != "" {
		// if a user-defined DENYLIST value has been provided, use that instead of any matched files
		denylist = f
	}

	// MANIFEST might be propagated here by the `match` function if it finds a matching manifest file,
	// so try that first
	manifest := os.Getenv(config.ManifestEnv)
	if f := c.String(config.ManifestParam); f != "" {
		// if a user-defined MANIFEST value has been provided, use that instead of any matched files
		manifest = f
	}

	nvddir := c.String(config.NVDDirParam)

	log.Infof("Using %s as denylist", denylist)
	log.Infof("Using %s as manifest file", manifest)
	log.Infof("Using %s as nvddir", nvddir)

	cvesToIgnore := make(map[string]bool)

	if _, err := os.Stat(denylist); os.IsNotExist(err) {
		log.Warnf("Denylist file with path '%s' does not exist, skipping", denylist)
	} else {
		cves, err := omnibus.ParseDenylist(denylist)
		if err != nil {
			log.Warnf("Could not read denylist %s, skipping", denylist)
		} else {
			log.Infof("CVEs to ignore: '%s'", strings.Join(cves, "', '"))

			for _, cve := range cves {
				cvesToIgnore[cve] = true
			}
		}
	}

	dependencies, err := omnibus.ParseVersionManifest(manifest)
	if err != nil {
		log.Errorf("Could not read manifest file %s", path)
		return nil, err
	}

	matcher := nvd.NewCVEMatcher(nvddir)
	matches, err := matcher.Match(dependencies)
	if err != nil {
		log.Errorf("Could not match NVD CVEs")
		return nil, err
	}

	result := make([]nvd.CVEMatch, 0, len(matches))
	for _, match := range matches {
		if cvesToIgnore[match.CVE] {
			continue
		}

		result = append(result, match)
	}

	data, err := json.Marshal(result)
	if err != nil {
		log.Errorf("Could not marshal matched CVEs")
		return nil, err
	}

	return io.NopCloser(bytes.NewReader(data)), nil
}
