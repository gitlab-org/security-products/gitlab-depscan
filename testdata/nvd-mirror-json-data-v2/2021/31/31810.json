{
  "cve": {
    "id": "CVE-2021-31810",
    "sourceIdentifier": "cve@mitre.org",
    "published": "2021-07-13T13:15:09.243",
    "lastModified": "2024-01-24T05:15:09.683",
    "vulnStatus": "Modified",
    "descriptions": [
      {
        "lang": "en",
        "value": "An issue was discovered in Ruby through 2.6.7, 2.7.x through 2.7.3, and 3.x through 3.0.1. A malicious FTP server can use the PASV response to trick Net::FTP into connecting back to a given IP address and port. This potentially makes curl extract information about services that are otherwise private and not disclosed (e.g., the attacker can conduct port scans and service banner extractions)."
      },
      {
        "lang": "es",
        "value": "Se ha detectado un problema en Ruby versiones hasta 2.6.7, versiones 2.7.x hasta 2.7.3, y versiones 3.x hasta 3.0.1. Un servidor FTP malicioso puede usar la respuesta PASV para engañar a la función Net::FTP para que se conecte de nuevo a una dirección IP y un puerto determinados. Esto potencialmente hace que curl extraiga información sobre servicios que de otra manera son privados y no son divulgados (por ejemplo, el atacante puede conducir escaneos de puertos y extracciones de banners de servicios)"
      }
    ],
    "metrics": {
      "cvssMetricV31": [
        {
          "source": "nvd@nist.gov",
          "type": "Primary",
          "cvssData": {
            "attackComplexity": "LOW",
            "attackVector": "NETWORK",
            "availabilityImpact": "NONE",
            "baseScore": 5.8,
            "baseSeverity": "MEDIUM",
            "confidentialityImpact": "LOW",
            "integrityImpact": "NONE",
            "privilegesRequired": "NONE",
            "scope": "CHANGED",
            "userInteraction": "NONE",
            "vectorString": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N",
            "version": "3.1"
          },
          "exploitabilityScore": 3.9,
          "impactScore": 1.4
        }
      ],
      "cvssMetricV30": null,
      "cvssMetricV2": [
        {
          "source": "nvd@nist.gov",
          "type": "Primary",
          "cvssData": {
            "accessComplexity": "LOW",
            "accessVector": "NETWORK",
            "authentication": "NONE",
            "availabilityImpact": "NONE",
            "baseScore": 5,
            "confidentialityImpact": "PARTIAL",
            "integrityImpact": "NONE",
            "vectorString": "AV:N/AC:L/Au:N/C:P/I:N/A:N",
            "version": "2.0"
          },
          "baseSeverity": "MEDIUM",
          "exploitabilityScore": 10,
          "impactScore": 2.9,
          "acInsufInfo": false,
          "obtainAllPrivilege": false,
          "obtainUserPrivilege": false,
          "obtainOtherPrivilege": false,
          "userInteractionRequired": false
        }
      ]
    },
    "weaknesses": [
      {
        "source": "nvd@nist.gov",
        "type": "Primary",
        "description": [
          {
            "lang": "en",
            "value": "NVD-CWE-Other"
          }
        ]
      }
    ],
    "configurations": [
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:a:ruby-lang:ruby:*:*:*:*:*:*:*:*",
                "matchCriteriaId": "EF4F2191-A77B-45F1-BA1E-F36F7F357704",
                "versionEndIncluding": "2.6.7"
              },
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:a:ruby-lang:ruby:*:*:*:*:*:*:*:*",
                "matchCriteriaId": "D29D5386-D306-4CB4-82EC-678319F0101D",
                "versionStartIncluding": "2.7.0",
                "versionEndIncluding": "2.7.3"
              },
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:a:ruby-lang:ruby:*:*:*:*:*:*:*:*",
                "matchCriteriaId": "528E25B8-22A4-4AAF-9582-76BCDF3705B5",
                "versionStartIncluding": "3.0.0",
                "versionEndIncluding": "3.0.1"
              }
            ]
          },
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": false,
                "criteria": "cpe:2.3:o:fedoraproject:fedora:34:*:*:*:*:*:*:*",
                "matchCriteriaId": "A930E247-0B43-43CB-98FF-6CE7B8189835"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:debian:debian_linux:9.0:*:*:*:*:*:*:*",
                "matchCriteriaId": "DEECE5FC-CACF-4496-A3E7-164736409252"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:a:oracle:jd_edwards_enterpriseone_tools:*:*:*:*:*:*:*:*",
                "matchCriteriaId": "D1298AA2-0103-4457-B260-F976B78468E7",
                "versionEndExcluding": "9.2.6.1"
              }
            ]
          }
        ]
      }
    ],
    "references": [
      {
        "url": "https://hackerone.com/reports/1145454",
        "source": "cve@mitre.org",
        "tags": [
          "Exploit",
          "Patch",
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://lists.debian.org/debian-lts-announce/2021/10/msg00009.html",
        "source": "cve@mitre.org",
        "tags": [
          "Mailing List",
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://lists.debian.org/debian-lts-announce/2023/04/msg00033.html",
        "source": "cve@mitre.org"
      },
      {
        "url": "https://lists.fedoraproject.org/archives/list/package-announce%40lists.fedoraproject.org/message/MWXHK5UUHVSHF7HTHMX6JY3WXDVNIHSL/",
        "source": "cve@mitre.org"
      },
      {
        "url": "https://security.gentoo.org/glsa/202401-27",
        "source": "cve@mitre.org"
      },
      {
        "url": "https://security.netapp.com/advisory/ntap-20210917-0001/",
        "source": "cve@mitre.org",
        "tags": [
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://www.oracle.com/security-alerts/cpuapr2022.html",
        "source": "cve@mitre.org",
        "tags": [
          "Patch",
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://www.ruby-lang.org/en/news/2021/07/07/trusting-pasv-responses-in-net-ftp/",
        "source": "cve@mitre.org",
        "tags": [
          "Vendor Advisory"
        ]
      }
    ],
    "data_type": "MITRE",
    "data_format": "CVE",
    "data_version": "4.0",
    "CVE_data_meta": {
      "ASSIGNER": "cve@mitre.org",
      "ID": "CVE-2021-31810"
    },
    "problemtype": {
      "problemtype_data": [
        {
          "description": [
            {
              "lang": "en",
              "value": "NVD-CWE-Other"
            }
          ]
        }
      ]
    },
    "description": {
      "description_data": [
        {
          "lang": "en",
          "value": "An issue was discovered in Ruby through 2.6.7, 2.7.x through 2.7.3, and 3.x through 3.0.1. A malicious FTP server can use the PASV response to trick Net::FTP into connecting back to a given IP address and port. This potentially makes curl extract information about services that are otherwise private and not disclosed (e.g., the attacker can conduct port scans and service banner extractions)."
        },
        {
          "lang": "es",
          "value": "Se ha detectado un problema en Ruby versiones hasta 2.6.7, versiones 2.7.x hasta 2.7.3, y versiones 3.x hasta 3.0.1. Un servidor FTP malicioso puede usar la respuesta PASV para engañar a la función Net::FTP para que se conecte de nuevo a una dirección IP y un puerto determinados. Esto potencialmente hace que curl extraiga información sobre servicios que de otra manera son privados y no son divulgados (por ejemplo, el atacante puede conducir escaneos de puertos y extracciones de banners de servicios)"
        }
      ]
    }
  },
  "configurations": {
    "CVE_data_version": "4.0",
    "nodes": [
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:a:ruby-lang:ruby:*:*:*:*:*:*:*:*",
            "versionEndIncluding": "2.6.7"
          },
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:a:ruby-lang:ruby:*:*:*:*:*:*:*:*",
            "versionStartIncluding": "2.7.0",
            "versionEndIncluding": "2.7.3"
          },
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:a:ruby-lang:ruby:*:*:*:*:*:*:*:*",
            "versionStartIncluding": "3.0.0",
            "versionEndIncluding": "3.0.1"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "cpe23Uri": "cpe:2.3:o:fedoraproject:fedora:34:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:debian:debian_linux:9.0:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:a:oracle:jd_edwards_enterpriseone_tools:*:*:*:*:*:*:*:*",
            "versionEndExcluding": "9.2.6.1"
          }
        ]
      }
    ]
  },
  "impact": {
    "baseMetricV2": {
      "cvssV2": {
        "accessComplexity": "LOW",
        "accessVector": "NETWORK",
        "authentication": "NONE",
        "availabilityImpact": "NONE",
        "baseScore": 5,
        "confidentialityImpact": "PARTIAL",
        "integrityImpact": "NONE",
        "vectorString": "AV:N/AC:L/Au:N/C:P/I:N/A:N",
        "version": "2.0"
      },
      "exploitabilityScore": 10,
      "impactScore": 2.9,
      "severity": "MEDIUM"
    },
    "baseMetricV3": {
      "cvssV3": {
        "attackComplexity": "LOW",
        "attackVector": "NETWORK",
        "availabilityImpact": "NONE",
        "baseScore": 5.8,
        "baseSeverity": "MEDIUM",
        "confidentialityImpact": "LOW",
        "integrityImpact": "NONE",
        "privilegesRequired": "NONE",
        "scope": "CHANGED",
        "userInteraction": "NONE",
        "vectorString": "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N",
        "version": "3.1"
      },
      "exploitabilityScore": 3.9,
      "impactScore": 1.4
    }
  },
  "publicationDate": "2021-07-13T13:15:09.243",
  "lastModifiedDate": "2024-01-24T05:15:09.683"
}
