{
  "cve": {
    "id": "CVE-2023-28322",
    "sourceIdentifier": "support@hackerone.com",
    "published": "2023-05-26T21:15:16.153",
    "lastModified": "2023-12-22T16:15:07.493",
    "vulnStatus": "Modified",
    "descriptions": [
      {
        "lang": "en",
        "value": "An information disclosure vulnerability exists in curl <v8.1.0 when doing HTTP(S) transfers, libcurl might erroneously use the read callback (`CURLOPT_READFUNCTION`) to ask for data to send, even when the `CURLOPT_POSTFIELDS` option has been set, if the same handle previously wasused to issue a `PUT` request which used that callback. This flaw may surprise the application and cause it to misbehave and either send off the wrong data or use memory after free or similar in the second transfer. The problem exists in the logic for a reused handle when it is (expected to be) changed from a PUT to a POST."
      }
    ],
    "metrics": {
      "cvssMetricV31": [
        {
          "source": "nvd@nist.gov",
          "type": "Primary",
          "cvssData": {
            "attackComplexity": "HIGH",
            "attackVector": "NETWORK",
            "availabilityImpact": "NONE",
            "baseScore": 3.7,
            "baseSeverity": "LOW",
            "confidentialityImpact": "LOW",
            "integrityImpact": "NONE",
            "privilegesRequired": "NONE",
            "scope": "UNCHANGED",
            "userInteraction": "NONE",
            "vectorString": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N",
            "version": "3.1"
          },
          "exploitabilityScore": 2.2,
          "impactScore": 1.4
        }
      ],
      "cvssMetricV30": null,
      "cvssMetricV2": null
    },
    "weaknesses": [
      {
        "source": "nvd@nist.gov",
        "type": "Primary",
        "description": [
          {
            "lang": "en",
            "value": "NVD-CWE-noinfo"
          }
        ]
      },
      {
        "source": "support@hackerone.com",
        "type": "Secondary",
        "description": [
          {
            "lang": "en",
            "value": "CWE-200"
          }
        ]
      }
    ],
    "configurations": [
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:a:haxx:curl:*:*:*:*:*:*:*:*",
                "matchCriteriaId": "376FCCEF-74BD-4A99-8A1E-B70A83D89E71",
                "versionEndExcluding": "8.1.0"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:fedoraproject:fedora:37:*:*:*:*:*:*:*",
                "matchCriteriaId": "E30D0E6F-4AE8-4284-8716-991DFA48CC5D"
              },
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:fedoraproject:fedora:38:*:*:*:*:*:*:*",
                "matchCriteriaId": "CC559B26-5DFC-4B7A-A27C-B77DE755DFF9"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:apple:macos:*:*:*:*:*:*:*:*",
                "matchCriteriaId": "FB5312D6-AEEA-4548-B3EF-B07B46168475",
                "versionStartIncluding": "11.0",
                "versionEndExcluding": "11.7.9"
              },
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:apple:macos:*:*:*:*:*:*:*:*",
                "matchCriteriaId": "5B763A1F-C183-4728-B593-67558FD9FC36",
                "versionStartIncluding": "12.0",
                "versionEndExcluding": "12.6.8"
              },
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:apple:macos:*:*:*:*:*:*:*:*",
                "matchCriteriaId": "3D701507-146E-4E5B-8C32-60E797E46627",
                "versionStartIncluding": "13.0",
                "versionEndExcluding": "13.5"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:a:netapp:clustered_data_ontap:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "1FE996B1-6951-4F85-AA58-B99A379D2163"
              },
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:a:netapp:ontap_antivirus_connector:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "759D1A24-B23B-404E-AD39-F18D7DBAD501"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:netapp:h300s_firmware:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "6770B6C3-732E-4E22-BF1C-2D2FD610061C"
              }
            ]
          },
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": false,
                "criteria": "cpe:2.3:h:netapp:h300s:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "9F9C8C20-42EB-4AB5-BD97-212DEB070C43"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:netapp:h500s_firmware:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "7FFF7106-ED78-49BA-9EC5-B889E3685D53"
              }
            ]
          },
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": false,
                "criteria": "cpe:2.3:h:netapp:h500s:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "E63D8B0F-006E-4801-BF9D-1C001BBFB4F9"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:netapp:h700s_firmware:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "56409CEC-5A1E-4450-AA42-641E459CC2AF"
              }
            ]
          },
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": false,
                "criteria": "cpe:2.3:h:netapp:h700s:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "B06F4839-D16A-4A61-9BB5-55B13F41E47F"
              }
            ]
          }
        ]
      },
      {
        "nodes": [
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": true,
                "criteria": "cpe:2.3:o:netapp:h410s_firmware:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "D0B4AD8A-F172-4558-AEC6-FF424BA2D912"
              }
            ]
          },
          {
            "operator": "OR",
            "negate": false,
            "cpeMatch": [
              {
                "vulnerable": false,
                "criteria": "cpe:2.3:h:netapp:h410s:-:*:*:*:*:*:*:*",
                "matchCriteriaId": "8497A4C9-8474-4A62-8331-3FE862ED4098"
              }
            ]
          }
        ]
      }
    ],
    "references": [
      {
        "url": "http://seclists.org/fulldisclosure/2023/Jul/47",
        "source": "support@hackerone.com",
        "tags": [
          "Mailing List",
          "Third Party Advisory"
        ]
      },
      {
        "url": "http://seclists.org/fulldisclosure/2023/Jul/48",
        "source": "support@hackerone.com",
        "tags": [
          "Mailing List",
          "Third Party Advisory"
        ]
      },
      {
        "url": "http://seclists.org/fulldisclosure/2023/Jul/52",
        "source": "support@hackerone.com",
        "tags": [
          "Mailing List",
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://hackerone.com/reports/1954658",
        "source": "support@hackerone.com",
        "tags": [
          "Exploit",
          "Patch",
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://lists.debian.org/debian-lts-announce/2023/12/msg00015.html",
        "source": "support@hackerone.com"
      },
      {
        "url": "https://lists.fedoraproject.org/archives/list/package-announce%40lists.fedoraproject.org/message/F4I75RDGX5ULSSCBE5BF3P5I5SFO7ULQ/",
        "source": "support@hackerone.com"
      },
      {
        "url": "https://lists.fedoraproject.org/archives/list/package-announce%40lists.fedoraproject.org/message/Z2LIWHWKOVH24COGGBCVOWDXXIUPKOMK/",
        "source": "support@hackerone.com"
      },
      {
        "url": "https://security.gentoo.org/glsa/202310-12",
        "source": "support@hackerone.com",
        "tags": [
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://security.netapp.com/advisory/ntap-20230609-0009/",
        "source": "support@hackerone.com",
        "tags": [
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://support.apple.com/kb/HT213843",
        "source": "support@hackerone.com",
        "tags": [
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://support.apple.com/kb/HT213844",
        "source": "support@hackerone.com",
        "tags": [
          "Third Party Advisory"
        ]
      },
      {
        "url": "https://support.apple.com/kb/HT213845",
        "source": "support@hackerone.com",
        "tags": [
          "Third Party Advisory"
        ]
      }
    ],
    "data_type": "MITRE",
    "data_format": "CVE",
    "data_version": "4.0",
    "CVE_data_meta": {
      "ASSIGNER": "support@hackerone.com",
      "ID": "CVE-2023-28322"
    },
    "problemtype": {
      "problemtype_data": [
        {
          "description": [
            {
              "lang": "en",
              "value": "NVD-CWE-noinfo"
            }
          ]
        },
        {
          "description": [
            {
              "lang": "en",
              "value": "CWE-200"
            }
          ]
        }
      ]
    },
    "description": {
      "description_data": [
        {
          "lang": "en",
          "value": "An information disclosure vulnerability exists in curl <v8.1.0 when doing HTTP(S) transfers, libcurl might erroneously use the read callback (`CURLOPT_READFUNCTION`) to ask for data to send, even when the `CURLOPT_POSTFIELDS` option has been set, if the same handle previously wasused to issue a `PUT` request which used that callback. This flaw may surprise the application and cause it to misbehave and either send off the wrong data or use memory after free or similar in the second transfer. The problem exists in the logic for a reused handle when it is (expected to be) changed from a PUT to a POST."
        }
      ]
    }
  },
  "configurations": {
    "CVE_data_version": "4.0",
    "nodes": [
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:a:haxx:curl:*:*:*:*:*:*:*:*",
            "versionEndExcluding": "8.1.0"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:fedoraproject:fedora:37:*:*:*:*:*:*:*"
          },
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:fedoraproject:fedora:38:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:apple:macos:*:*:*:*:*:*:*:*",
            "versionStartIncluding": "11.0",
            "versionEndExcluding": "11.7.9"
          },
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:apple:macos:*:*:*:*:*:*:*:*",
            "versionStartIncluding": "12.0",
            "versionEndExcluding": "12.6.8"
          },
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:apple:macos:*:*:*:*:*:*:*:*",
            "versionStartIncluding": "13.0",
            "versionEndExcluding": "13.5"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:a:netapp:clustered_data_ontap:-:*:*:*:*:*:*:*"
          },
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:a:netapp:ontap_antivirus_connector:-:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:netapp:h300s_firmware:-:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "cpe23Uri": "cpe:2.3:h:netapp:h300s:-:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:netapp:h500s_firmware:-:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "cpe23Uri": "cpe:2.3:h:netapp:h500s:-:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:netapp:h700s_firmware:-:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "cpe23Uri": "cpe:2.3:h:netapp:h700s:-:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "vulnerable": true,
            "cpe23Uri": "cpe:2.3:o:netapp:h410s_firmware:-:*:*:*:*:*:*:*"
          }
        ]
      },
      {
        "operator": "OR",
        "cpe_match": [
          {
            "cpe23Uri": "cpe:2.3:h:netapp:h410s:-:*:*:*:*:*:*:*"
          }
        ]
      }
    ]
  },
  "impact": {
    "baseMetricV2": {
      "cvssV2": {}
    },
    "baseMetricV3": {
      "cvssV3": {
        "attackComplexity": "HIGH",
        "attackVector": "NETWORK",
        "availabilityImpact": "NONE",
        "baseScore": 3.7,
        "baseSeverity": "LOW",
        "confidentialityImpact": "LOW",
        "integrityImpact": "NONE",
        "privilegesRequired": "NONE",
        "scope": "UNCHANGED",
        "userInteraction": "NONE",
        "vectorString": "CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N",
        "version": "3.1"
      },
      "exploitabilityScore": 2.2,
      "impactScore": 1.4
    }
  },
  "publicationDate": "2023-05-26T21:15:16.153",
  "lastModifiedDate": "2023-12-22T16:15:07.493"
}
