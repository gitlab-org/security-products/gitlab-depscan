package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	cweinfo "gitlab.com/gitlab-org/security-products/cwe-info-go"

	"gitlab-depscan/cvss"
	"gitlab-depscan/logutils"
	config "gitlab-depscan/metadata"
	"gitlab-depscan/nvd"
)

type vulnerability struct {
	cve, packageName, URL string
}

type vulnerabilitiesWithSeverity map[report.SeverityLevel][]vulnerability

func addVulnerabilityWithSeverity(vuln report.DependencyScanningVulnerability, vulns vulnerabilitiesWithSeverity) {
	vulnerability := vulnerability{
		cve:         vuln.Identifiers[0].Value,
		packageName: vuln.Location.Dependency.Package.Name,
		URL:         vuln.Links[0].URL,
	}

	severityVal := vuln.Severity
	vulns[severityVal] = append(vulns[severityVal], vulnerability)
}

func logVulnerabilities(vulns vulnerabilitiesWithSeverity) {
	severityLevels := []report.SeverityLevel{
		report.SeverityLevelCritical,
		report.SeverityLevelHigh,
		report.SeverityLevelMedium,
		report.SeverityLevelLow,
		report.SeverityLevelInfo,
		report.SeverityLevelUnknown,
	}

	for _, severityLevel := range severityLevels {
		vulnerabilitiesForSeverityLevel := vulns[severityLevel]

		for _, vulnerability := range vulnerabilitiesForSeverityLevel {
			msg := fmt.Sprintf("Found %s for '%s' with severity '%s': %s",
				vulnerability.cve,
				vulnerability.packageName,
				severityLevel.String(),
				vulnerability.URL,
			)
			logutils.LogVulnerability(msg, severityLevel)
		}
	}
}

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	data, err := io.ReadAll(reader)
	if err != nil {
		return nil, fmt.Errorf("failed to read analyzer report: %w", err)
	}

	var cves []nvd.CVEMatch
	if err := json.Unmarshal(data, &cves); err != nil {
		return nil, fmt.Errorf("failed to unmarshal analyzer report: %w", err)
	}

	useOmnibusPaths := parseBoolEnvVar(config.UseOmnibusPathsEnv)
	manifestPath := os.Getenv(config.ManifestEnv)
	location := filepath.Join(prependPath, manifestPath)
	conv := newConverter(useOmnibusPaths, location, cves)
	report, err := conv.run()
	if err != nil {
		return nil, fmt.Errorf("creating dependency scanning security report: %v", err)
	}

	return &report, nil
}

func parseBoolEnvVar(name string) bool {
	val := os.Getenv(name)
	// Ignore the error and return default value of false
	out, _ := strconv.ParseBool(strings.ToLower(val))
	return out
}

type converter struct {
	cves                        []nvd.CVEMatch
	report                      report.Report
	vulnerabilitiesWithSeverity map[report.SeverityLevel][]vulnerability
	location                    string
	useOmnibusPaths             bool
}

func newConverter(useOmnibusPaths bool, location string, cves []nvd.CVEMatch) *converter {
	return &converter{
		vulnerabilitiesWithSeverity: make(map[report.SeverityLevel][]vulnerability),
		location:                    location,
		cves:                        cves,
		report:                      report.NewReport(),
		useOmnibusPaths:             useOmnibusPaths,
	}
}

func (c *converter) run() (report.Report, error) {
	c.report.Scan.Analyzer = config.AnalyzerDetails

	if c.useOmnibusPaths {
		log.Infoln("Generating report with Omnibus software paths")
		c.convert(func(product string) string { return fmt.Sprintf("config/software/%s.rb", url.QueryEscape(product)) })
	} else {
		log.Infoln("Generating report with default paths")
		c.convert(func(string) string { return c.location })
	}

	return c.report, nil
}

func (c *converter) addVulnerability(cve nvd.CVEMatch, location string) report.Dependency {
	cwe, err := cweinfo.GetCweInfo(strings.TrimPrefix(cve.CWE, "CWE-"))
	if err != nil {
		log.Warnf("Could not get info for %s: %v", cve.CWE, err)
	}

	dep := report.Dependency{
		Package: report.Package{
			Name: filepath.Join(cve.CPEVendor, cve.CPEProduct),
		},
		Version: cve.Version,
	}

	v := report.DependencyScanningVulnerability{
		Vulnerability: report.Vulnerability{
			Category: report.CategoryDependencyScanning,
			Scanner:  &config.IssueScanner,
			Name:     cwe.Title, // no package name
			Severity: cvss.Severity(cve.CVSS),
			Location: report.Location{
				File:       location,
				Dependency: &dep,
			},
			Identifiers: []report.Identifier{report.CVEIdentifier(cve.CVE)},
			Links: report.NewLinks(
				fmt.Sprintf("https://nvd.nist.gov/vuln/detail/%s", cve.CVE),
			),
		},
	}

	c.report.Vulnerabilities = append(c.report.Vulnerabilities, v.ToVulnerability())

	addVulnerabilityWithSeverity(v, c.vulnerabilitiesWithSeverity)

	return dep
}

func (c *converter) convert(fnLocation func(product string) string) {
	seen := make(map[string]*report.DependencyFile)

	for _, cve := range c.cves {
		location := fnLocation(cve.CPEProduct)

		dep := c.addVulnerability(cve, location)

		locationDeps, ok := seen[location]
		if !ok {
			locationDeps = &report.DependencyFile{
				Path:           location,
				PackageManager: config.PackageManager,
			}
			seen[location] = locationDeps
		}

		found := false
		for _, locationDep := range locationDeps.Dependencies {
			if dep.Package == locationDep.Package && dep.Version == locationDep.Version {
				found = true
				break
			}
		}
		if !found {
			locationDeps.Dependencies = append(locationDeps.Dependencies, dep)
		}
	}

	for _, locationDeps := range seen {
		c.report.DependencyFiles = append(c.report.DependencyFiles, *locationDeps)
	}

	logVulnerabilities(c.vulnerabilitiesWithSeverity)
}
