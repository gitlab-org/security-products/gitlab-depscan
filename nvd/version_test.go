package nvd_test

import (
	"fmt"
	"gitlab-depscan/nvd"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCanonicalSemver(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{"0", "v0.0.0"},
		{"1", "v1.0.0"},
		{"1.0", "v1.0.0"},
		{"1.0.0", "v1.0.0"},
		{"v1", "v1.0.0"},
		{"1.2.3g", "v1.2.3-g"},
		{"1.2.3g+foo", "v1.2.3-g"},
		{"1+foo", "v1.0.0"},
		{"6.4-20230225", "v6.4.0-20230225"},
		{"20240619", "v20240619.0.0"},
		{"20120601-3.0", "v20120601.0.0-3.0"},
		{"2.1.5.1", "v2.1.5-1"},
		{"  v  1  . 2   . 3  - lol   + hey", "v1.2.3-lol"},
		{"a0", ""},
		{"", ""},
		{"0.9_b", "v0.9.0-b"},
		{"0.9.5_d", "v0.9.5-d"},

		{"master", ""},                           // Git branch
		{"main", ""},                             // Git branch
		{"4ac3cc368e73f3750591493104798fce", ""}, // SHA1-like
		{"c9092d05347c925a26f6887980e185206e13f9d6", ""}, // SHA1
	}

	for _, tc := range tests {
		t.Run(tc.input, func(t *testing.T) {
			actual := nvd.CanonicalSemver(tc.input)
			require.Equal(t, tc.expected, actual)
		})
	}
}

func TestLikelyComparesDateAndMajor(t *testing.T) {
	tests := []struct {
		v        string
		w        string
		expected bool
	}{
		{"", "", false},
		{"v1.0.0", "", false},
		{"", "v1.0.0", false},
		{"v1.0.0", "v1.0.0", false},
		{"v1.0.0", "v5.0.0", false},
		{"v1.0.0", "v2024.0.0", true},
		{"v2024.0.0", "v1.0.0", true},
		{"v2024.0.0", "v2020.0.0", false},
		{"v1.0.0", nvd.CanonicalSemver("2024-12-24"), true},
		{"v1.0.0", nvd.CanonicalSemver("20241224"), true},
		{nvd.CanonicalSemver("2024-12-24"), nvd.CanonicalSemver("20241224"), true},
		{nvd.CanonicalSemver("2024-01-19"), nvd.CanonicalSemver("2024-12-24"), false},
		{nvd.CanonicalSemver("20240119"), nvd.CanonicalSemver("20241224"), false},
		{nvd.CanonicalSemver("20240119"), nvd.CanonicalSemver("20200101"), false},
	}

	for _, tc := range tests {
		t.Run(fmt.Sprintf("%s and %s", tc.v, tc.w), func(t *testing.T) {
			actual := nvd.LikelyComparesDateAndMajor(tc.v, tc.w)
			require.Equal(t, tc.expected, actual)
		})
	}
}
