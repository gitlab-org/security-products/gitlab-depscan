package nvd

import (
	"fmt"
	"gitlab-depscan/omnibus"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"golang.org/x/mod/semver"
)

// CVEMatcher represents a matcher on a given nvd-mirror-json-data-v2 directory
type CVEMatcher struct {
	dir string
}

// NewCVEMatcher returns a matcher for the given nvd-mirror-json-data-v2 directory
func NewCVEMatcher(dir string) *CVEMatcher {
	return &CVEMatcher{
		dir: dir,
	}
}

// Match walks through all CVEs and returns the ones matching the given dependencies
func (m *CVEMatcher) Match(deps []omnibus.UsedDependency) ([]CVEMatch, error) {
	if m.dir == "" {
		return nil, fmt.Errorf("no NVD CVE directory was provided")
	}

	files, err := filepath.Glob(filepath.Join(m.dir, "*/*/*.json"))
	if err != nil {
		return nil, err
	}

	if len(files) == 0 {
		return nil, fmt.Errorf("no NVD CVEs were found")
	}

	log.Infof("Matching %d dependencies against %d CVEs", len(deps), len(files))

	added := make(map[CVEMatch]bool)
	var result []CVEMatch

	for _, file := range files {
		data, err := os.ReadFile(file)
		if err != nil {
			return nil, err
		}

		cves, err := NewCVEs(data)
		if err != nil {
			return nil, err
		}

		for _, cve := range cves {
			for _, dep := range deps {
				if match(cve, dep) {
					m := CVEMatch{
						CPEVendor:  cve.CPEVendor,
						CPEProduct: cve.CPEProduct,
						Version:    dep.Version,
						CVE:        cve.ID,
						CWE:        cve.CWE,
						CVSS:       cve.CVSS,
					}

					if !added[m] {
						added[m] = true
						result = append(result, m)
					}
				}
			}
		}
	}

	return result, nil
}

func match(cve CVE, dep omnibus.UsedDependency) bool {
	if dep.Name != cve.CPEProduct {
		return false
	}

	if dep.Vendor != "" && dep.Vendor != cve.CPEVendor {
		return false
	}

	depVer := CanonicalSemver(dep.Version)

	if depVer == "" {
		// invalid version, default to match
		return false
	}

	if cve.CPEVersion != "" && semver.Compare(cve.CPEVersion, depVer) != 0 {
		return false
	}

	if cve.VersionStartExcluding != "" && semver.Compare(cve.VersionStartExcluding, depVer) != -1 {
		return false
	}

	if cve.VersionStartIncluding != "" && semver.Compare(cve.VersionStartIncluding, depVer) == 1 {
		return false
	}

	if cve.VersionEndExcluding != "" {
		if semver.Compare(cve.VersionEndExcluding, depVer) != 1 || LikelyComparesDateAndMajor(cve.VersionEndExcluding, depVer) {
			return false
		}
	}

	if cve.VersionEndIncluding != "" {
		if semver.Compare(cve.VersionEndIncluding, depVer) == -1 || LikelyComparesDateAndMajor(cve.VersionEndIncluding, depVer) {
			return false
		}
	}

	return true
}

// CVEMatch represents a match against a specific CVE vulnerable configuration
type CVEMatch struct {
	CPEVendor  string `json:"cpe_vendor"`
	CPEProduct string `json:"cpe_product"`
	Version    string `json:"version"`
	CVE        string `json:"cve"`
	CWE        string `json:"cwe,omitempty"`
	CVSS       string `json:"cvss,omitempty"`
}
