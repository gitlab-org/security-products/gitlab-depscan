package nvd_test

import (
	"encoding/json"
	"fmt"
	"gitlab-depscan/nvd"
	"gitlab-depscan/omnibus"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestCVEMatcher(t *testing.T) {
	var report report.Report
	data, err := os.ReadFile("../test/expect/gl-dependency-scanning-report.json")
	require.NoError(t, err)
	err = json.Unmarshal(data, &report)
	require.NoError(t, err)

	expected := make(map[string]bool)
	for _, vuln := range report.Vulnerabilities {
		var cve string
		for _, id := range vuln.Identifiers {
			if id.Type == "cve" && strings.HasPrefix(id.Value, "CVE-") {
				cve = id.Value
				break
			}
		}

		dep := vuln.Location.Dependency
		expected[fmt.Sprintf("%s:%s:%s", dep.Package.Name, dep.Version, cve)] = true
	}

	matcher := nvd.NewCVEMatcher("../testdata/nvd-mirror-json-data-v2")
	deps, err := omnibus.ParseVersionManifest("../test/fixtures/version-manifest.json")
	require.NoError(t, err)
	matches, err := matcher.Match(deps)
	require.NoError(t, err)

	actual := make(map[string]bool)
	for _, match := range matches {
		actual[fmt.Sprintf("%s/%s:%s:%s", match.CPEVendor, match.CPEProduct, match.Version, match.CVE)] = true
	}

	require.Equal(t, expected, actual)
}
