package nvd

import (
	"encoding/json"
	"strings"

	"golang.org/x/mod/semver"
)

// NewCVEs unmarshals a CVE, with each configuration yielding a dedicated entry
func NewCVEs(data []byte) ([]CVE, error) {
	var nvdCVE nvdCVE
	if err := json.Unmarshal(data, &nvdCVE); err != nil {
		return nil, err
	}

	// only consider the first CWE, assigned by NVD
	var cwe string
	problemtypeData := nvdCVE.CVE.Problemtype.ProblemtypeData
	if len(problemtypeData) > 0 {
		descriptions := problemtypeData[0].Description
		if len(descriptions) > 0 {
			value := descriptions[0].Value
			if strings.HasPrefix(value, "CWE-") {
				cwe = value
			}
		}
	}

	cvss := nvdCVE.Impact.BaseMetricV3.CVSSV3.VectorString
	if cvss == "" {
		cvss = nvdCVE.Impact.BaseMetricV2.CVSSV2.VectorString
	}

	var result []CVE

	nodes := append([]nvdNode{}, nvdCVE.Configurations.Nodes...)

	for len(nodes) > 0 {
		node := nodes[0]
		nodes = nodes[1:]
		nodes = append(nodes, node.Children...)

		for _, match := range node.CPEMatch {
			if !match.Vulnerable {
				continue
			}

			restCpe, found := strings.CutPrefix(match.CPE23URI, "cpe:2.3:a:")
			if !found {
				continue
			}

			vendor, restCpe, found := strings.Cut(restCpe, ":")
			if !found {
				continue
			}

			product, restCpe, found := strings.Cut(restCpe, ":")
			if !found {
				continue
			}

			version, _, found := strings.Cut(restCpe, ":")
			if !found {
				continue
			}

			if version == "*" {
				version = ""
			} else if version == "-" {
				// no match
				continue
			} else if version != "" {
				version = CanonicalSemver(version)
				if version == "" {
					// don't promote fixed match into wildcard
					continue
				}
			}

			versionStartExcluding := CanonicalSemver(match.VersionStartExcluding)
			versionStartIncluding := CanonicalSemver(match.VersionStartIncluding)
			versionEndExcluding := CanonicalSemver(match.VersionEndExcluding)
			versionEndIncluding := CanonicalSemver(match.VersionEndIncluding)

			// check for unwanted unbounded promotions
			if (match.VersionEndExcluding != "" && versionEndExcluding == "") || (match.VersionEndIncluding != "" && versionEndIncluding == "") {
				continue
			}

			// force at most one start and end
			start := versionStartIncluding
			if versionStartIncluding != "" {
				if versionStartExcluding != "" {
					versionStartExcluding = ""
				}
			} else {
				start = versionStartExcluding
			}

			end := versionEndExcluding
			if versionEndExcluding != "" {
				if versionEndIncluding != "" {
					versionEndIncluding = ""
				}
			} else {
				end = versionEndIncluding
			}

			// force prerelease if start is greater than end
			if semver.Compare(start, end) == 1 && semver.Prerelease(end) != "" && semver.Prerelease(start) == "" {
				start = start + "-\x00"

				if semver.Compare(start, end) == -1 {
					// fixed, store back
					if versionStartIncluding != "" {
						versionStartIncluding = start
					} else {
						versionStartExcluding = start
					}
				}
			}

			result = append(result, CVE{
				ID:                    nvdCVE.CVE.CVEDataMeta.ID,
				CWE:                   cwe,
				CVSS:                  cvss,
				CPEVendor:             vendor,
				CPEProduct:            product,
				CPEVersion:            version,
				VersionStartExcluding: versionStartExcluding,
				VersionStartIncluding: versionStartIncluding,
				VersionEndExcluding:   versionEndExcluding,
				VersionEndIncluding:   versionEndIncluding,
			})
		}
	}

	return result, nil
}

// CVE represents one specific vulnerable configuration of an advisory
type CVE struct {
	ID                    string
	CWE                   string
	CVSS                  string
	CPEVendor             string
	CPEProduct            string
	CPEVersion            string
	VersionStartExcluding string
	VersionStartIncluding string
	VersionEndExcluding   string
	VersionEndIncluding   string
}

type nvdCVE struct {
	CVE struct {
		CVEDataMeta struct {
			ID string `json:"ID"`
		} `json:"CVE_data_meta"`
		Problemtype struct {
			ProblemtypeData []struct {
				Description []struct {
					Value string `json:"value"`
				} `json:"description"`
			} `json:"problemtype_data"`
		} `json:"problemtype"`
	} `json:"cve"`
	Configurations struct {
		Nodes []nvdNode `json:"nodes"`
	} `json:"configurations"`
	Impact struct {
		BaseMetricV3 struct {
			CVSSV3 struct {
				VectorString string `json:"vectorString"`
			} `json:"cvssV3"`
		} `json:"baseMetricV3"`
		BaseMetricV2 struct {
			CVSSV2 struct {
				VectorString string `json:"vectorString"`
			} `json:"cvssV2"`
		} `json:"baseMetricV2"`
	} `json:"impact"`
}

type nvdNode struct {
	Children []nvdNode `json:"children"`
	CPEMatch []struct {
		Vulnerable            bool   `json:"vulnerable"`
		CPE23URI              string `json:"cpe23Uri"`
		VersionStartExcluding string `json:"versionStartExcluding"`
		VersionStartIncluding string `json:"versionStartIncluding"`
		VersionEndExcluding   string `json:"versionEndExcluding"`
		VersionEndIncluding   string `json:"versionEndIncluding"`
	} `json:"cpe_match"`
}
