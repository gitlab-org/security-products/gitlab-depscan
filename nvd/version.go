package nvd

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"golang.org/x/mod/semver"
)

// CanonicalSemver turns a version into its canonical semver representation
// in a way that is more tolerant than `semver.Canonical(string)`
func CanonicalSemver(version string) string {
	if len(version) > 50 {
		// unreasonably long
		return ""
	}

	// drop `v` prefix
	version = strings.TrimSpace(version)
	version = strings.TrimPrefix(version, "v")
	version = strings.TrimSpace(version)

	// drop `+build` suffix
	if offset := strings.LastIndexByte(version, '+'); offset != -1 {
		version = version[:offset]
		version = strings.TrimSpace(version)
	}

	if match, _ := regexp.MatchString("^[a-zA-Z]+$", version); match {
		// Git branch like
		return ""
	}

	if match, _ := regexp.MatchString("^[0-9A-Fa-f]{20,}$", version); match {
		// SHA-like
		return ""
	}

	if len(version) == 0 || version[0] < '0' || version[0] > '9' {
		// doesn't start with digit
		return ""
	}

	// extract major/minor/patch
	major, version := extractNumber(version)
	minor, version := extractNumber(version)
	patch, version := extractNumber(version)

	// extract prerelease
	version = strings.TrimLeft(version, "-_")
	version = strings.TrimSpace(version)
	prerelease := version

	// reconstruct
	if prerelease != "" {
		version = fmt.Sprintf("v%d.%d.%d-%s", major, minor, patch, prerelease)
	} else {
		version = fmt.Sprintf("v%d.%d.%d", major, minor, patch)
	}

	return semver.Canonical(version)
}

// LikelyComparesDateAndMajor returns true if the major version difference
// between canonical versions `v` and `w` is greater than 3 orders of magnitude,
// indicating that a date might be compared against a normal version number for example
func LikelyComparesDateAndMajor(v, w string) bool {
	if v == "" || w == "" {
		return false
	}

	m1 := len(semver.Major(v))
	m2 := len(semver.Major(w))

	if m2 > m1 {
		return m2-m1 >= 3
	}

	return m1-m2 >= 3
}

func extractNumber(s string) (int, string) {
	offset := 0
	for offset < len(s) && s[offset] >= '0' && s[offset] <= '9' {
		offset++
	}

	if offset == 0 {
		return 0, s
	}

	result, _ := strconv.Atoi(s[:offset])
	s = s[offset:]
	s = strings.TrimSpace(s)
	s = strings.TrimLeft(s, ".")
	s = strings.TrimSpace(s)

	return result, s
}
