package nvd_test

import (
	"gitlab-depscan/nvd"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCVE(t *testing.T) {
	tests := []struct {
		file     string
		expected []nvd.CVE
	}{
		{
			"../testdata/CVE-2023-2251.json",
			[]nvd.CVE{
				{
					ID:                    "CVE-2023-2251",
					CWE:                   "CWE-248",
					CVSS:                  "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:             "yaml_project",
					CPEProduct:            "yaml",
					CPEVersion:            "",
					VersionStartIncluding: "v2.0.0-5",
					VersionEndExcluding:   "v2.2.2",
				},
			},
		},
		{
			"../testdata/CVE-2019-0190.json",
			[]nvd.CVE{
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "oracle",
					CPEProduct: "retail_xstore_point_of_service",
					CPEVersion: "v7.1.0",
				},
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "oracle",
					CPEProduct: "retail_xstore_point_of_service",
					CPEVersion: "v7.0.0",
				},
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "oracle",
					CPEProduct: "hospitality_guest_access",
					CPEVersion: "v4.2.0",
				},
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "oracle",
					CPEProduct: "hospitality_guest_access",
					CPEVersion: "v4.2.1",
				},
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "oracle",
					CPEProduct: "enterprise_manager_ops_center",
					CPEVersion: "v12.3.3",
				},
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "oracle",
					CPEProduct: "instantis_enterprisetrack",
					CPEVersion: "v17.1.0",
				},
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "oracle",
					CPEProduct: "instantis_enterprisetrack",
					CPEVersion: "v17.2.0",
				},
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "oracle",
					CPEProduct: "instantis_enterprisetrack",
					CPEVersion: "v17.3.0",
				},
				{
					ID:         "CVE-2019-0190",
					CWE:        "",
					CVSS:       "CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H",
					CPEVendor:  "apache",
					CPEProduct: "http_server",
					CPEVersion: "v2.4.37",
				},
			},
		},
		{
			"../testdata/CVE-2009-2943.json",
			[]nvd.CVE{
				{
					ID:         "CVE-2009-2943",
					CWE:        "",
					CVSS:       "AV:N/AC:L/Au:N/C:P/I:P/A:P",
					CPEVendor:  "ocaml",
					CPEProduct: "postgresql-ocaml",
					CPEVersion: "v1.5.4",
				},
				{
					ID:         "CVE-2009-2943",
					CWE:        "",
					CVSS:       "AV:N/AC:L/Au:N/C:P/I:P/A:P",
					CPEVendor:  "ocaml",
					CPEProduct: "postgresql-ocaml",
					CPEVersion: "v1.7.0",
				},
				{
					ID:         "CVE-2009-2943",
					CWE:        "",
					CVSS:       "AV:N/AC:L/Au:N/C:P/I:P/A:P",
					CPEVendor:  "ocaml",
					CPEProduct: "postgresql-ocaml",
					CPEVersion: "v1.12.1",
				},
			},
		},
		{
			"../testdata/CVE-2019-1543.json",
			[]nvd.CVE{
				{
					ID:                    "CVE-2019-1543",
					CWE:                   "CWE-327",
					CVSS:                  "CVSS:3.0/AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N",
					CPEVendor:             "openssl",
					CPEProduct:            "openssl",
					CPEVersion:            "",
					VersionStartIncluding: "v1.1.1-\x00",
					VersionEndIncluding:   "v1.1.1-b",
				},
				{
					ID:                    "CVE-2019-1543",
					CWE:                   "CWE-327",
					CVSS:                  "CVSS:3.0/AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N",
					CPEVendor:             "openssl",
					CPEProduct:            "openssl",
					CPEVersion:            "",
					VersionStartIncluding: "v1.1.0-\x00",
					VersionEndIncluding:   "v1.1.0-j",
				},
			},
		},
		{
			"../testdata/CVE-2022-3734.json",
			nil,
		},
		{
			"../testdata/CVE-2005-0953.json",
			[]nvd.CVE{
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v1.0.0",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v0.9.5-a",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v0.9.5-d",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v0.9.5-c",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v0.9.0-a",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v0.9.0-c",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v1.0.2",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v0.9.0",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v1.0.1",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v0.9.0-b",
				},
				{
					ID:         "CVE-2005-0953",
					CWE:        "",
					CVSS:       "AV:L/AC:H/Au:N/C:P/I:P/A:P",
					CPEVendor:  "bzip",
					CPEProduct: "bzip2",
					CPEVersion: "v0.9.5-b",
				},
			},
		},
		{
			"../testdata/CVE-2022-41741.json",
			[]nvd.CVE{
				{
					ID:                    "CVE-2022-41741",
					CWE:                   "CWE-787",
					CVSS:                  "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H",
					CPEVendor:             "f5",
					CPEProduct:            "nginx_ingress_controller",
					CPEVersion:            "",
					VersionStartIncluding: "v2.0.0",
					VersionEndIncluding:   "v2.4.0",
				},
				{
					ID:                    "CVE-2022-41741",
					CWE:                   "CWE-787",
					CVSS:                  "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H",
					CPEVendor:             "f5",
					CPEProduct:            "nginx_ingress_controller",
					CPEVersion:            "",
					VersionStartIncluding: "v1.9.0",
					VersionEndIncluding:   "v1.12.4",
				},
				{
					ID:         "CVE-2022-41741",
					CWE:        "CWE-787",
					CVSS:       "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H",
					CPEVendor:  "f5",
					CPEProduct: "nginx",
					CPEVersion: "v1.23.1",
				},
				{
					ID:         "CVE-2022-41741",
					CWE:        "CWE-787",
					CVSS:       "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H",
					CPEVendor:  "f5",
					CPEProduct: "nginx",
					CPEVersion: "v1.23.0",
				},
				{
					ID:                    "CVE-2022-41741",
					CWE:                   "CWE-787",
					CVSS:                  "CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H",
					CPEVendor:             "f5",
					CPEProduct:            "nginx",
					CPEVersion:            "",
					VersionStartIncluding: "v1.1.3",
					VersionEndIncluding:   "v1.22.0",
				},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.file, func(t *testing.T) {
			data, err := os.ReadFile(tc.file)
			require.NoError(t, err)

			actual, err := nvd.NewCVEs(data)
			require.NoError(t, err)

			require.Equal(
				t,
				tc.expected,
				actual,
			)
		})
	}
}
