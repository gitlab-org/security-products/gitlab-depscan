# gitlab-depscan

`gitlab-depscan` scans the dependencies of
[omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab).

`gitlab-despcan` checks the version manifest produced by [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab) to see
whether (OS/Infra) dependencies have known vulnerabilities. The tool can be
used as a [Dependency Scanning
analyzer](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
or as a standalone script; it takes as input a version manifest
file that contains a list of dependencies, a CVE denylist that contains a
list of CVEs that should be excluded from the report, and a path to
the directory containing NVD CVEs.

For every dependency/version, `gitlab-depscan` checks whether there exists a
corresponding CVE. If a vulnerability is detected, it will automatically
generate a report that adheres to the [Dependency Scanning Report
Format](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#reports-json-format).

The explanation below assumes that you're running `gitlab-depscan` from the
Docker container

## Running the analyzer locally

The docker image can be used as a DS analyzer but also contains
a script that mimics the old behavior of `gitlab-depscan` which you can use to
run it locally.

### Analyzer

`gitlab-despcan` is shipped as part of a Docker image which can be
executed as an analyzer by running the command below.

``` bash
docker run -e CI_PROJECT_DIR="/code" \
    -v "$PWD:/code" \
    -it \
    --rm registry.gitlab.com/gitlab-org/security-products/gitlab-depscan:edge
```

The analyzer searches for a manifest file `version-manifest.json` and a CVE
denylist file `.cveignore` in the `code` directory and generates dependency
scanning report file.

### Script

The `gitlab-depscan.sh` script is shipped as part of the Docker container and
mimics the old behaviour of `gitlab-depscan.sh`; it is just a wrapper around
the `gitlab-depscan` analyzer. You can invoke the script by executing the
command below. `gitlab-depscan.sh` takes the path to the version manifest file
as first parameter, the path to the denylist file and the path to the NVD directory.
Due to the flag `-v "$PWD:/code"`, the files from the current working directory are
mapped into the `/code` directory of the Docker container which is the reason why the
parameters for `gitlab-depscan.sh` have to carry the `/code` prefix.

``` bash
docker run -e CI_PROJECT_DIR="/code" \
    -v "$PWD:/code" \
    -it \
    --rm registry.gitlab.com/gitlab-org/security-products/gitlab-depscan:edge \
    /gitlab-depscan.sh /code/testdata/version-manifest.json /code/testdata/denylist.txt /code/testdata/nvd-mirror-json-data-v2
```

## Output

The script generates two reports:

- `gl-dependency-scanning-report.json` is a Dependency Scanning JSON artifact.
- `dependency_report.txt` is a plain text report.

The report vulnerability details will be logged to stdout.

### Version Manifest Format

gitlab-depscan supports the input format below. It extracts the product and
version information from the software dictionary and the `display_version`
field, respectively.

```json
{
  "manifest_format": 2,
  "software": {
    "ncurses": {
      "locked_version": "5.9",
      "locked_source": {
        "md5": "8cb9c412e5f2d96bc6f459aa8c6282a1",
        "url": "http://ftp.gnu.org/gnu/ncurses/ncurses-5.9.tar.gz"
      },
      "display_version": "5.9",
      "source_type": "url",
      "described_version": "5.9",
      "license": "MIT"
    },
    "nginx": {
      "locked_version": "1.2",
      "locked_source": {
        "md5": "9cb9g412e5f2d16b16f439aa8c6282b2",
        "url": "http://ftp.gnu.org/gnu/ncurses/ncurses-1.2.tar.gz"
      },
      "display_version": "1.2",
      "source_type": "url",
      "described_version": "1.2",
      "license": "MIT"
    }
  },
  "build_version": "12.2.4+rnightly.125492.5032ed10",
  "build_git_revision": "5032ed10c547382d5ae19aea89d9f15dad202003",
  "license": "MIT"
}
```

### Denylist Format

```
# Here is a comment
CVE-2019-15548

# Here is another comment
CVE-2019-17594
```

Comments and empty lines are supported in this file.
Ignored CVEs are removed from the reports and standard output.

## Implementation

This analyzer is written in Go using the [common
library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common
library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.

